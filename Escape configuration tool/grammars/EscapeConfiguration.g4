grammar EscapeConfiguration;

options {
	language=Java;
}

@header {
package econfig.lexparse;
}

/* Parser Rules */
configuration 			:	coordinateType dimensions (locationInitializers | pieceDescriptors | rules)* EOF;

coordinateType			:	COORDINATE TYPE? ':' coordinateName ;
coordinateName			:	(HEX | SQUARE | TRIANGLE) ;

dimensions				: 	dimension+ ;
dimension				:	AXIS ':' INTEGER ;

locationInitializers	:	LOCATIONS ':' locationList ;
locationList			:	location+ ;
location				:	coordinate locationType? piece?	;
locationType			:	BLOCK | CLEAR | EXIT ;

pieceDescriptors		:	PIECE DESCRIPTORS ':' pieceDescriptorList ;
pieceDescriptorList		:	pieceDescriptor+ ;
pieceDescriptor			:	pieceName movementPattern attributes ;

rules                   :   RULES ':' ruleList ;
ruleList                :   gameRule+ ;
gameRule                :   ruleName ruleValue? ;
ruleName                :   POINT_CONFLICT | REMOVE | SCORE | TURN_LIMIT ;
ruleValue               :   INTEGER ;

attributes				:	'[' attributeList ']' ;
attributeList			:	attribute (',' attribute)* ;
attribute				:	valuedAttribute | unvaluedAttribute;
valuedAttribute			: 	attributeName attributeValue ;
unvaluedAttribute		:	attributeName ;

attributeName			:	DISTANCE | FLY | JUMP | STACK | UNBLOCK | VALUE ;
attributeValue			:	INTEGER ;

piece					:	PLAYER pieceName ;
pieceName				:	BIRD | DOG | FROG | HORSE | SNAIL ;

coordinate				:	'(' coord ',' coord ')' ;
coord					:	'-'? INTEGER ;

movementPattern			:	DIAGONAL | LINEAR | OMNI | ORTHOGONAL | SKEW ;


/* Lexer Rules */ 
/* Reserved words */
AXIS					:	'xMax' | 'yMax' ;
BIRD					:	'BIRD' | 'bird' ;
BLOCK					:	'BLOCK' | 'block' ;
CLEAR					:	'CLEAR' | 'clear' ;
COORDINATE				:	'Coordinate' | 'coordinate' ;
DIAGONAL				:	'DIAGONAL' | 'diagonal' ;
DESCRIPTORS				:	'Descriptors' | 'descriptors' ;
DISTANCE				:	'DISTANCE' | 'distance' ;
DOG						:	'DOG' | 'dog' ;
EXIT					:	'EXIT' | 'exit' ;
FROG					:	'FROG' | 'frog' ;
FLY						:	'FLY' | 'fly' ;
HEX						:	'HEX' | 'hex' ;
HORSE					:	'HORSE' | 'horse' ;
JUMP					:	'JUMP' | 'jump' ;
LINEAR					:	'LINEAR' | 'linear' ;
LOCATIONS				:	'Locations' | 'locations' ;
OMNI					:	'OMNI' | 'omni' ;
ORTHOGONAL				:	'ORTHOGONAL' | 'orthogonal' ;
//ORTHOSQUARE				:	'ORTHOSQUARE' | 'orthosquare' ;
PIECE					:	'Piece' | 'piece' ;
RULES                   :   'Rules' | 'rules' ;
POINT_CONFLICT          :   'POINT_CONFLICT' | 'point_conflict';
REMOVE                  :   'REMOVE' | 'remove' ;
SCORE                   :   'SCORE' | 'score' ;
SKEW                    :   'SKEW' | 'skew' ;
SNAIL					:	'SNAIL' | 'snail' ;
SQUARE					:	'SQUARE' | 'square' ;
STACK                   :   'STACK' | 'stack' ;
TRIANGLE				:	'TRIANGLE' | 'triangle' ;
TURN_LIMIT              :   'TURN_LIMIT' | 'turn_limit' ;
TYPE					:	'Type' | 'type' ;
UNBLOCK					:	'UNBLOCK' | 'unblock' ;
VALUE					:	'VALUE' | 'value' ;

PLAYER					:	PLAYERNAME[12] ;

WS 						:	[ \t\r\n\f]+ -> skip ;
COMMENT  				:	'/*' (COMMENT | .)*? '*/' -> skip ;

INTEGER					:	DIGIT+ ;
LETTER					:	[a-zA-Z] ;

fragment DIGIT			:	[0-9] ;
fragment PLAYERNAME		:	'PLAYER' | 'player' ;
