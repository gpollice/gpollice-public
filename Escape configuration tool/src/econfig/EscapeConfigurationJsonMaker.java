/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * 
 * Copyright ©2020 Gary F. Pollice
 *******************************************************************************/

package econfig;

import java.util.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.*;
import econfig.lexparse.EscapeConfigurationBaseVisitor;
import econfig.lexparse.EscapeConfigurationParser.*;

/**
 * Tree visitor that produces a JSON representation of the input .egc file.
 * @version Jun 26, 2020
 */
public class EscapeConfigurationJsonMaker extends EscapeConfigurationBaseVisitor<String>
{
	private StringBuilder configuration;
	private boolean hasXaxis = false, hasYaxis = false;
	private List<String> errors;
	private String coordinateType = "NONE";
	private List<String> currentAttributes;
	private Set<String> locations;
	private String indentString = "  ";
	private int indentLevel = 0;

	public EscapeConfigurationJsonMaker()
	{
		configuration = new StringBuilder();
		errors = new LinkedList<String>();
		currentAttributes = new ArrayList<String>();
		locations = new HashSet<String>();
		indentLevel = 0;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitConfiguration(econfig.lexparse
	 * .EscapeConfigurationParser.ConfigurationContext)
	 */
	@Override
	public String visitConfiguration(ConfigurationContext ctx)
	{
		configuration.append("{\n");
		super.visitConfiguration(ctx); // visit all the children
		configuration.append("}\n");
		if (!errors.isEmpty()) {
			System.err.println("Errors dected on input");
			for (String error : errors) {
				System.err.println("  " + error);
			}
			throw new ConfigurationException("Errors on input were found, check stderr");
		}
		return configuration.toString();
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinateType(econfig.
	 * lexparse.EscapeConfigurationParser.CoordinateTypeContext)
	 */
	@Override
	public String visitCoordinateType(CoordinateTypeContext ctx)
	{
		configuration.append("  \"coordinateType\" : \"");
		coordinateType = visitCoordinateName(ctx.coordinateName());
		configuration.append(coordinateType);
		configuration.append("\",\n");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinateName(econfig.
	 * lexparse.EscapeConfigurationParser.CoordinateNameContext)
	 */
	@Override
	public String visitCoordinateName(CoordinateNameContext ctx)
	{
		return ctx.getText().toUpperCase();
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitDimension(econfig.lexparse.
	 * EscapeConfigurationParser.DimensionContext)
	 */
	@Override
	public String visitDimension(DimensionContext ctx)
	{
		String axis = ctx.AXIS().getText();
		configuration.append("  \"" + axis + "\" : ");
		configuration.append(ctx.INTEGER().getText());
		configuration.append(",\n");
		axisCheck(ctx.AXIS());
		return null;
	}

	private void axisCheck(TerminalNode axis)
	{
		if (axis.getText().equals("xMax")) {
			if (hasXaxis) {
				addError(axis, "xMax declared twice");
			} else {
				hasXaxis = true;
			}
		} else {
			if (hasYaxis) {
				addError(axis, "yMax declared twice");
			} else {
				hasYaxis = true;
			}
		}
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocationInitializers(econfig.
	 * lexparse.EscapeConfigurationParser.LocationInitializersContext)
	 */
	@Override
	public String visitLocationInitializers(LocationInitializersContext ctx)
	{
		configuration.append("\n  \"locationInitializers\" : [\n");
		super.visitLocationInitializers(ctx);
		configuration.append("  ],\n");
		return null;
	}
	

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocationList(econfig.lexparse.EscapeConfigurationParser.LocationListContext)
	 */
	@Override
	public String visitLocationList(LocationListContext ctx)
	{
		int nLocations = ctx.location().size();
		for (LocationContext c : ctx.location()) {
			visitLocation(c);
			if (--nLocations > 0) {
				configuration.append(",\n");
			}
		}
		return null;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocation(econfig.lexparse.
	 * EscapeConfigurationParser.LocationContext)
	 */
	@Override
	public String visitLocation(LocationContext ctx)
	{
		configuration.append("    {");
		String coordinate = visitCoordinate(ctx.coordinate());
		String locationType = null;
		String piece = null;
		configuration.append(coordinate);
		if (ctx.getChildCount() == 1) {
			addError(ctx, "Location is missing piece description or location type");
		} else {
			configuration.append(", \"locationType\" : ");
			if (ctx.locationType() == null) {
				configuration.append("null");
			} else {
				locationType = visitLocationType(ctx.locationType());
				configuration.append(locationType);
			}
			configuration.append(", ");
			if (ctx.piece() == null) {
				configuration.append("\"player\" : null, \"pieceName\" : null");
			} else {
				piece = visitPiece(ctx.piece());
				configuration.append(piece);
			}
		}
		configuration.append("}");
		if (piece != null && locationType != null
			&& (locationType.contains("BLOCK") || locationType.contains("EXIT"))) {
			addError(ctx, "You cannot place a piece on " + coordinate
				+ " since it is BLOCKED or an EXIT");
		}
		return null;
	}
	
	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocationType(econfig.lexparse.
	 * EscapeConfigurationParser.LocationTypeContext)
	 */
	@Override
	public String visitLocationType(LocationTypeContext ctx)
	{
		return '"' + ctx.getChild(0).getText().toUpperCase() + '"';
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceDescriptors(econfig.
	 * lexparse.EscapeConfigurationParser.PieceDescriptorsContext)
	 */
	@Override
	public String visitPieceDescriptors(PieceDescriptorsContext ctx)
	{
		configuration.append("\n\n  \"pieceTypes\" : [\n");
		super.visitPieceDescriptors(ctx);
		configuration.append("\n  ],\n");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceDescriptorList(econfig.lexparse.EscapeConfigurationParser.PieceDescriptorListContext)
	 */
	@Override
	public String visitPieceDescriptorList(PieceDescriptorListContext ctx)
	{
		int nDescriptors = ctx.pieceDescriptor().size();
		for (PieceDescriptorContext c : ctx.pieceDescriptor()) {
			visitPieceDescriptor(c);
			if (--nDescriptors > 0) {
				configuration.append(",\n");
			}
		}
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceDescriptor(econfig.
	 * lexparse.EscapeConfigurationParser.PieceDescriptorContext)
	 */
	@Override
	public String visitPieceDescriptor(PieceDescriptorContext ctx)
	{
		currentAttributes.clear();
		configuration.append("    {");
		configuration.append("\"pieceName\" : ");
		configuration.append('"' + ctx.pieceName().accept(this) + "\", ");
		configuration.append("\"movementPattern\" : ");
		configuration.append('"' + ctx.movementPattern().accept(this) + "\", \n");
		configuration.append("      \"attributes\" : [");
		ctx.attributes().accept(this);
		configuration.append("] }");
		pieceDescriptorCheck(ctx);
		return null;
	}
	
	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitAttributeList(econfig.lexparse.EscapeConfigurationParser.AttributeListContext)
	 */
	@Override
	public String visitAttributeList(AttributeListContext ctx)
	{
		int nAttributes = ctx.attribute().size();
		for (AttributeContext c : ctx.attribute()) {
			visitAttribute(c);
			if (--nAttributes > 0) {
				configuration.append(", ");
			}
		};
		return null;
	}

	@Override
	public String visitUnvaluedAttribute(UnvaluedAttributeContext ctx)
	{
		configuration.append("{\"id\" : ");
		String attributeID = ctx.attributeName().getText().toUpperCase();
		currentAttributes.add(attributeID);
		configuration.append('"' + attributeID + "\", \"value\" : 0}");
		return null;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitValuedAttribute(econfig.lexparse.
	 * EscapeConfigurationParser.ValuedAttributeContext)
	 */
	@Override
	public String visitValuedAttribute(ValuedAttributeContext ctx)
	{
		configuration.append("{\"id\" : ");
		String attributeID = ctx.attributeName().getText().toUpperCase();
		currentAttributes.add(attributeID);
		configuration.append('"' + attributeID + "\", \"value\" : " + ctx.attributeValue().getText() + '}');
		String attributeValue = ctx.attributeValue().getText();
		attributeCheck(ctx, attributeID, attributeValue);
		return null;
	}
	
	private void attributeCheck(ParserRuleContext ctx, String name, String value)
	{
		int attrValue = Integer.parseInt(value);
		if (attrValue == 0) {
			addError(ctx, "this attribute must have a positive value");
		}
	}
	
	private void  pieceDescriptorCheck(ParserRuleContext ctx)
	{
		if (!(currentAttributes.contains("FLY") ^ currentAttributes.contains("DISTANCE"))) {
			addError(ctx, " piece must contain exactly one of FLY or DISTANCE");
		}
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitRules(econfig.lexparse.
	 * EscapeConfigurationParser.RulesContext)
	 */
	@Override
	public String visitRules(RulesContext ctx)
	{
		configuration.append("\n  \"rules\" : [\n");
		visitChildren(ctx);
		configuration.append("  ]\n");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitRuleList(econfig.lexparse.EscapeConfigurationParser.RuleListContext)
	 */
	@Override
	public String visitRuleList(RuleListContext ctx)
	{
		int nRules = ctx.gameRule().size();
		for (GameRuleContext c : ctx.gameRule()) {
			visitGameRule(c);
			if (--nRules > 0) {
				configuration.append(',');
			}
			configuration.append('\n');
		};
		return null;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitGameRule(econfig.lexparse.
	 * EscapeConfigurationParser.GameRuleContext)
	 */
	@Override
	public String visitGameRule(GameRuleContext ctx)
	{
		configuration.append("    {");
		configuration.append("\"id\" : \"" 
			+ ctx.ruleName().getText().toUpperCase() + "\", \"intValue\" : ");
		if (ctx.ruleValue() != null) {
			configuration.append(ctx.ruleValue().getText());
		} else {
			configuration.append('0');
		}
		configuration.append("}");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPiece(econfig.lexparse.
	 * EscapeConfigurationParser.PieceContext)
	 */
	@Override
	public String visitPiece(PieceContext ctx)
	{
		return "\"player\" : \"" + ctx.PLAYER().getText().toUpperCase() + "\", "
				+ "\"pieceName\" : \"" + ctx.pieceName().accept(this) + '"';
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceName(econfig.lexparse.
	 * EscapeConfigurationParser.PieceNameContext)
	 */
	@Override
	public String visitPieceName(PieceNameContext ctx)
	{
		return ctx.getText().toUpperCase();
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinate(econfig.lexparse.
	 * EscapeConfigurationParser.CoordinateContext)
	 */
	@Override
	public String visitCoordinate(CoordinateContext ctx)
	{
		String c = "\"x\" : " + ctx.coord(0).getText() + ", \"y\" : " 
			+ ctx.coord(1).getText();
		if (!locations.add(c)) {
			addError(ctx, "this location has been defined before");
		}
		return c;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitMovementPattern(econfig.
	 * lexparse.EscapeConfigurationParser.MovementPatternContext)
	 */
	@Override
	public String visitMovementPattern(MovementPatternContext ctx)
	{
		String mp = ctx.getText().toUpperCase();
		movementPatternCheck(ctx, mp);
		return mp;
	}
	
	private void movementPatternCheck(ParserRuleContext ctx, String mp)
	{
		switch (mp) {
			case "ORTHOGONAL":
				if (coordinateType.equals("HEX") || coordinateType.equals("TRIANGLE")) {
					addError(ctx, mp + " is not compatible with HEX coordinates");
				}
				break;
			case "DIAGONAL":
				if (!coordinateType.equals("SQUARE")) {
					addError(ctx, mp + " is not compatible with " + coordinateType + " coordinates");
				}
				break;
		}
	}
	
	private void addError(ParserRuleContext ctx, String msg)
	{
		errors.add("Line " + ctx.getStart().getLine() + ": " + msg);
	}
	
	private void addError(TerminalNode n, String msg)
	{
		errors.add("Line " + n.getSymbol().getLine() + ": " + msg);
	}
	
	private void indent()
	{
		configuration.append("\n");
		for (int i = 0; i < indentLevel; i++) {
			configuration.append(indentString);
		}
	}

	/**
	 * @return the configuration
	 */
	public String getConfiguration()
	{
		return configuration.toString();
	}
}
