/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design. The course was
 * taken at Worcester Polytechnic Institute. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse Public License
 * v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Copyright ©2016 Gary F. Pollice
 *******************************************************************************/
package econfig;

import java.util.*;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.*;
import econfig.lexparse.EscapeConfigurationBaseVisitor;
import econfig.lexparse.EscapeConfigurationParser.*;

/**
 * Description
 * 
 * @version May 24, 2020
 */
public class EscapeConfigurationXmlMaker extends EscapeConfigurationBaseVisitor<String>
{
	private StringBuilder configuration;
	private boolean hasXaxis = false, hasYaxis = false;
	private List<String> errors;
	private String coordinateType = "NONE";
	private List<String> currentAttributes;
	private Set<String> locations;

	public EscapeConfigurationXmlMaker()
	{
		configuration = new StringBuilder();
		errors = new LinkedList<String>();
		currentAttributes = new ArrayList<String>();
		locations = new HashSet<String>();
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitConfiguration(econfig.lexparse
	 * .EscapeConfigurationParser.ConfigurationContext)
	 */
	@Override
	public String visitConfiguration(ConfigurationContext ctx)
	{
		configuration.append(
				"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
						+ "<escapeGameInitializer>\n\n"
						+ "    <!-- Board items -->\n");
		super.visitConfiguration(ctx); // visit all the children
		configuration.append("</escapeGameInitializer>\n");
		if (!errors.isEmpty()) {
			System.err.println("Errors dected on input");
			for (String error : errors) {
				System.err.println("  " + error);
			}
			throw new ConfigurationException("Errors on input were found, check stderr");
		}
		return configuration.toString();
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinateType(econfig.
	 * lexparse.EscapeConfigurationParser.CoordinateTypeContext)
	 */
	@Override
	public String visitCoordinateType(CoordinateTypeContext ctx)
	{
		configuration.append("    <coordinateType>");
		coordinateType = visitCoordinateName(ctx.coordinateName());
		configuration.append(coordinateType);
		configuration.append("</coordinateType>\n");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinateName(econfig.
	 * lexparse.EscapeConfigurationParser.CoordinateNameContext)
	 */
	@Override
	public String visitCoordinateName(CoordinateNameContext ctx)
	{
		return ctx.getText().toUpperCase();
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitDimension(econfig.lexparse.
	 * EscapeConfigurationParser.DimensionContext)
	 */
	@Override
	public String visitDimension(DimensionContext ctx)
	{
		configuration.append("    <");
		String axis = ctx.AXIS().getText();
		configuration.append(axis + ">");
		configuration.append(ctx.INTEGER().getText());
		configuration.append("</" + axis + ">\n");
		axisCheck(ctx.AXIS());
		return null;
	}

	private void axisCheck(TerminalNode axis)
	{
		if (axis.getText().equals("xMax")) {
			if (hasXaxis) {
				addError(axis, "xMax declared twice");
			} else {
				hasXaxis = true;
			}
		} else {
			if (hasYaxis) {
				addError(axis, "yMax declared twice");
			} else {
				hasYaxis = true;
			}
		}
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocationInitializers(econfig.
	 * lexparse.EscapeConfigurationParser.LocationInitializersContext)
	 */
	@Override
	public String visitLocationInitializers(LocationInitializersContext ctx)
	{
		configuration.append("\n"
				+ "    <!-- Locations -->\n");
		return super.visitLocationInitializers(ctx);
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocation(econfig.lexparse.
	 * EscapeConfigurationParser.LocationContext)
	 */
	@Override
	public String visitLocation(LocationContext ctx)
	{
		configuration.append("    <locationInitializers>");
		
		String coordinate = visitCoordinate(ctx.coordinate());
		configuration.append("\n        " + coordinate);
		
		String locationType = null;
		if (ctx.locationType() != null) {
			locationType = visitLocationType(ctx.locationType());
			configuration.append("\n        " + locationType);
		}
		
		String piece = null;
		if (ctx.piece()!= null) {
			piece = ctx.piece().accept(this);
			configuration.append("\n        " + piece);
		}
		configuration.append("\n    </locationInitializers>\n");

		if (piece != null && locationType != null
		  && (locationType.contains("BLOCK") || locationType.contains("EXIT"))) {
			addError(ctx, "You cannot place a piece on " + coordinate 
				+ " since it is BLOCKED or an EXIT");
		}
		return null;
	}
	
	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitLocationType(econfig.lexparse.
	 * EscapeConfigurationParser.LocationTypeContext)
	 */
	@Override
	public String visitLocationType(LocationTypeContext ctx)
	{
		return "<locationType>" + ctx.getChild(0).getText().toUpperCase()
				+ "</locationType>";
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceDescriptors(econfig.
	 * lexparse.EscapeConfigurationParser.PieceDescriptorsContext)
	 */
	@Override
	public String visitPieceDescriptors(PieceDescriptorsContext ctx)
	{
		configuration.append("\n    <!-- Piece descriptors -->");
		visitChildren(ctx);
		configuration.append("\n");
		return null;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceDescriptor(econfig.
	 * lexparse.EscapeConfigurationParser.PieceDescriptorContext)
	 */
	@Override
	public String visitPieceDescriptor(PieceDescriptorContext ctx)
	{
		currentAttributes.clear();
		configuration.append("\n    <pieceTypes>");
		configuration.append("\n        <pieceName>" + ctx.pieceName().accept(this)
				+ "</pieceName>");
		configuration.append("\n        <movementPattern>" + ctx.movementPattern().accept(this)
				+ "</movementPattern>");
		ctx.attributes().accept(this);
		configuration.append("\n    </pieceTypes>");
		pieceDescriptorCheck(ctx);
		return null;
	}
	
	private void  pieceDescriptorCheck(ParserRuleContext ctx)
	{
		if (!(currentAttributes.contains("FLY") ^ currentAttributes.contains("DISTANCE"))) {
			addError(ctx, " piece must contain exactly one of FLY or DISTANCE");
		}
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitRules(econfig.lexparse.
	 * EscapeConfigurationParser.RulesContext)
	 */
	@Override
	public String visitRules(RulesContext ctx)
	{
		configuration.append("\n    <!-- Game Rules -->");
		visitChildren(ctx);
		configuration.append("\n");
		return null;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitGameRule(econfig.lexparse.
	 * EscapeConfigurationParser.GameRuleContext)
	 */
	@Override
	public String visitGameRule(GameRuleContext ctx)
	{
		configuration.append("\n    <rules>");
		configuration.append("\n        <id>" 
			+ ctx.ruleName().getText().toUpperCase() + "</id>");
		if (ctx.ruleValue() != null) {
			configuration.append("\n        <value>"
				+ ctx.ruleValue().getText() + "</value>");
		}
		configuration.append("\n    </rules>");
		return null;
	}
	
	@Override
	public String visitUnvaluedAttribute(UnvaluedAttributeContext ctx)
	{
		configuration.append("\n        <attributes>");
		String attributeID = ctx.attributeName().getText().toUpperCase();
		currentAttributes.add(attributeID);
		configuration.append("\n            <id>" + attributeID + "</id>");
		configuration.append("\n        </attributes>");
		return null;
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitValuedAttribute(econfig.lexparse.
	 * EscapeConfigurationParser.ValuedAttributeContext)
	 */
	@Override
	public String visitValuedAttribute(ValuedAttributeContext ctx)
	{
		configuration.append("\n        <attributes>");
		String attributeID = ctx.attributeName().getText().toUpperCase();
		currentAttributes.add(attributeID);
		String attributeValue = ctx.attributeValue() == null ? "0"
			: ctx.attributeValue().getText();
		configuration.append("\n            <id>" + attributeID + "</id>");
		configuration.append("\n            <value>" + attributeValue + "</value>");
		configuration.append("\n        </attributes>");
		attributeCheck(ctx, attributeID, attributeValue);
		return null;
	}
	
	private void attributeCheck(ParserRuleContext ctx, String name, String value)
	{
		int attrValue = Integer.parseInt(value);
		if (attrValue == 0) {
			addError(ctx, "this attribute must have a positive value");
		}
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitPiece(econfig.lexparse.
	 * EscapeConfigurationParser.PieceContext)
	 */
	@Override
	public String visitPiece(PieceContext ctx)
	{
		return "<player>" + ctx.PLAYER().getText().toUpperCase()
				+ "</player><pieceName>" + ctx.pieceName().accept(this)
				+ "</pieceName>";
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitPieceName(econfig.lexparse.
	 * EscapeConfigurationParser.PieceNameContext)
	 */
	@Override
	public String visitPieceName(PieceNameContext ctx)
	{
		return ctx.getText().toUpperCase();
	}

	/*
	 * @see
	 * econfig.lexparse.EscapeConfigurationBaseVisitor#visitCoordinate(econfig.lexparse.
	 * EscapeConfigurationParser.CoordinateContext)
	 */
	@Override
	public String visitCoordinate(CoordinateContext ctx)
	{
		String c = "<x>" + ctx.coord(0).getText() + "</x><y>" + ctx.coord(1).getText() + "</y>";
		if (!locations.add(c)) {
			addError(ctx, "this location has been defined before");
		}
		return c;
	}

	/*
	 * @see econfig.lexparse.EscapeConfigurationBaseVisitor#visitMovementPattern(econfig.
	 * lexparse.EscapeConfigurationParser.MovementPatternContext)
	 */
	@Override
	public String visitMovementPattern(MovementPatternContext ctx)
	{
		String mp = ctx.getText().toUpperCase();
		movementPatternCheck(ctx, mp);
		return mp;
	}
	
	private void movementPatternCheck(ParserRuleContext ctx, String mp)
	{
		switch (mp) {
			case "ORTHOGONAL":
				if (coordinateType.equals("HEX") || coordinateType.equals("TRIANGLE")) {
					addError(ctx, mp + " is not compatible with HEX coordinates");
				}
				break;
			case "DIAGONAL":
				if (!coordinateType.equals("SQUARE")) {
					addError(ctx, mp + " is not compatible with " + coordinateType + " coordinates");
				}
				break;
		}
	}
	
	private void addError(ParserRuleContext ctx, String msg)
	{
		errors.add("Line " + ctx.getStart().getLine() + ": " + msg);
	}
	
	private void addError(TerminalNode n, String msg)
	{
		errors.add("Line " + n.getSymbol().getLine() + ": " + msg);
	}

	/**
	 * @return the configuration
	 */
	public String getConfiguration()
	{
		return configuration.toString();
	}

}
