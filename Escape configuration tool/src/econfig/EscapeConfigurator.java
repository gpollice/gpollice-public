/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design.
 * The course was taken at Worcester Polytechnic Institute.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 * 
 * Copyright ©2016-2020 Gary F. Pollice
 *******************************************************************************/
package econfig;

import java.io.IOException;
import java.nio.file.*;
import java.util.*;
import org.antlr.v4.runtime.*;
import econfig.lexparse.*;

/**
 * Description
 * @version May 23, 2020
 */
public class EscapeConfigurator
{
	private EscapeConfigurationBaseVisitor<String> configMaker;
	private ParserRuleContext parseTree;
	private String outputDirectory;
	private boolean useStdOut;
	
	public EscapeConfigurator()
	{	
		this(new EscapeConfigurationXmlMaker());
	}
	
	public EscapeConfigurator(EscapeConfigurationBaseVisitor<String> maker)
	{
		this.configMaker = maker;
		outputDirectory = ".";
		useStdOut = false;
	}
	
	public EscapeConfigurationLexer makeLexer(CharStream input)
	{
		return new EscapeConfigurationLexer(input);
	}
	
	public EscapeConfigurationParser makeParser(EscapeConfigurationLexer lexer)
	{
		CommonTokenStream tokenStream = new CommonTokenStream(lexer);
		return new EscapeConfigurationParser(tokenStream);
	}
	
	public ParserRuleContext parse(CharStream input)
	{
		parseTree =  makeParser(makeLexer(input)).configuration();
		return parseTree;
	}
	
	public String makeConfiguration(CharStream input)
	{
		parse(input);
		return parseTree.accept(configMaker);
	}
	
	private void showHelp()
    {
        System.out.println("Usage: EscapeConfigurator [ options ] [ sourcefiles ]\n"
            + "Options: \n"
            + "    -d <output directory>\n"
            + "    -s Display results on stdout\n"
            + "    -h Display this message\n");
    }
	
	private List<String> processOptions(String[] args)
	{
		List<String> newArgs = new LinkedList<String>();
		int i = 0;
		while (i < args.length) {
			String s = args[i++];
			switch (s) {
				case "-d":
					outputDirectory = args[i++];
	                break;
				case "-h":
					showHelp();
					System.exit(0);
				case "-s":
					useStdOut = true;
					break;
				default:
					newArgs.add(s);	// Input file
					break;
			}
		}
		return newArgs;
	}
	
	private void generateConfiguration(String fileName) throws IOException
	{
		String s = makeConfiguration(CharStreams.fromFileName(fileName));
		if (useStdOut) {
			System.out.println(s);
		} else {
			int ix = fileName.lastIndexOf('.');
			String xmlName = ix == -1 ? fileName : fileName.substring(0, ix);
			String out = outputDirectory + "/" + xmlName + ".xml";
//			System.err.println(">>> Output directory = " + outputDirectory);
			System.err.println(">>> Writing: " + out);
			Files.write(Paths.get(out), s.getBytes());
		}
	}
	
	public void run(String[] args) throws IOException
	{
		List<String> newArgs = processOptions(args);
		if (newArgs.isEmpty()) {
		    System.err.println("No input file");
		    System.exit(1);
		}
		for (String s : newArgs) {
			generateConfiguration(s);
		}
	}
	
	/**
	 * Run this program from the command line
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException
	{
		EscapeConfigurator configurator = new EscapeConfigurator();
		configurator.run(args);
	}
	
}
