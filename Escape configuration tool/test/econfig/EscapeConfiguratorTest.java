/*******************************************************************************
 * This files was developed for CS4233: Object-Oriented Analysis & Design. The course was
 * taken at Worcester Polytechnic Institute. All rights reserved. This program and the
 * accompanying materials are made available under the terms of the Eclipse Public License
 * v1.0 which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html Copyright ©2016 Gary F. Pollice
 *******************************************************************************/

package econfig;

import static org.junit.jupiter.api.Assertions.*;
import java.io.IOException;
import org.antlr.v4.runtime.CharStreams;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Description
 * 
 * @version May 24, 2020
 */
class EscapeConfiguratorTest
{
    private EscapeConfigurator configurator;

    @BeforeEach
    void setup()
    {
        // configurator = new EscapeConfigurator(new EscapeConfigurationJsonMaker());
        configurator = new EscapeConfigurator();
    }

    @ParameterizedTest
    @ValueSource(strings = {
        "MasterSquareGame.egc", "test1.egc"
    })
    void useTestFiles(String f) throws IOException
    {
        String configuration = configurator
            .makeConfiguration(CharStreams.fromFileName("config/" + f));
        System.out.println(configuration);
    }

    @Test
    void useString()
    {
        String s = "Coordinate type : SQUARE\n" + "xMax : 25\n" + "yMax : 20\n"
            + "\n" + "Locations :\n" + "    (3, 5) block\n"
            + "    (4, 4) clear player1 snail\n" + "    (5, 12) exit\n" + "    \n"
            + "Piece descriptors :\n" + "    SNAIL omni [distance 1]\n"
            + "    DOG linear [distance 5]\n"
            + "    HORSE diagonal [distance 7, jump]\n" + "    bird linear [fly 5]\n"
            + "    frog omni [distance 3, unblock]\n" + "    \n" + "Rules :\n"
            + "    REMOVE\n" + "    SCORE 10\n" + "    TURN_LIMIT 20";
        System.out.print(configurator.makeConfiguration(CharStreams.fromString(s)));
        assertTrue(true);
    }
}
