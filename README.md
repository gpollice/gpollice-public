# Gary Pollice's public repository #

This repository contains some projects, usually Eclipse projects, 
that I use in some of my course work and other development projects.
You may find these useful or might just be curious. I decided to put this
here for students and others who might be ihterested in some of the things
I do. Feel free to use any of the material here for your work. I would appreciate
attribution when you do.

Submit any bugs to the issues.

## Contents

#### Escape configuration tool ####

This project is a small Domain-Specific Language that describes games in the Escape
game that I uwe for the CS4233: Object-Oriented Analysis & Design class. It translates
a simple language into XML (I'm working on JSON). THe output is used to configure a
specific version of the Escape game. 

The project uses ANTLR for the language procesing. Currently, ANTLR 4.9 is used.